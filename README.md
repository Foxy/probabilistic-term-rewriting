# On Term Rewriting in the Presence of Probabilistic Effects

My thesis as a bachelor student in Computer Science at the University of
Bologna. Its main focus is probabilistic term rewriting and termination.
The fulltext is also [available on the institutional theses
repository](https://amslaurea.unibo.it/29074/) of the University of Bologna.
My supervisor was [Professor Dal
Lago](https://www.unibo.it/sitoweb/ugo.dallago).

The thesis is exclusively available in Italian. This repository includes:

- the Standard ML programs (invoke them from the `src/` directory itself);
- the work's $\LaTeX$ source code (the resulting PDF is published
  [here](https://foxy.codeberg.page/probabilistic-term-rewriting/thesis.pdf));
- the defense's slides' $\LaTeX$ source code (the resulting PDF is published
  [here](https://foxy.codeberg.page/probabilistic-term-rewriting/slides.pdf)).
