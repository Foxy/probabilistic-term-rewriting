use "traat/library.ML";
use "traat/termorders.ML";
use "multidistributions.sml";

(* ('a * 'a) -> order *)
fun geq ord (x, y) = ord (x, y) = GR orelse x = y

(* term multidistribution -> term -> (term * term -> ord)
   -> term multidistribution *)
fun maxJ mu a ord = (filter (fn (_, bi) => ord (a, bi) = GR) mu)

(* (term * term -> order) -> term * term multidistribution -> real -> ord *)
fun mpo ord (a, mu) epsilon =
  (forall (fn (pi, bi) => pi <= 0 orelse geq ord (a, bi)) mu)
  andalso
  (totalProbability (maxJ mu a ord) >= epsilon)

(* (term * term -> order) -> term * term multidistribution -> real ->
   (term -> real) -> ord *)
fun mpo ord (a, mu) epsilon f =
  (forall (fn (pi, bi) => pi <= 0 orelse f a >= f bi) mu)
  andalso
  (totalProbability (maxJ mu a ord) >= epsilon)
