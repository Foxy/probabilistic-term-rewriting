(* Generatore pseudocasuale *)
val rand = Random.rand (0, 0)

(* Estrae l'indice di un elemento a caso della lista *)
fun randomIndex l = Random.randRange (0, length l) rand

(* Separa l'elemento alla data posizione dal resto della lista *)
fun pick l index =
  (List.nth (l, index), List.take (l, index) @ List.drop (l, index + 1))

(* Separa un elemento casuale dal resto della lista *)
fun randomPick l = pick l (randomIndex l)

(* Partiziona la lista usando un pivot casuale *)
fun randomPartition l =
  let
    val (pivot, rest) = randomPick l
    val (left, right) = List.partition (fn x => x < pivot) rest
  in
    (left, pivot, right)
  end

(* Ordina le sottoliste sinistra e destra, poi vi frappone il pivot *)
fun sortPartitions (left, pivot, right) = rqSort left @ [pivot] @ rqSort right

(* Quicksort casuale (implementazione non in loco) *)
and rqSort [] = []
  | rqSort l = sortPartitions (randomPartition l);
