use "geo.sml";

(* int -> int *)
fun double 0 = 0
  | double (n + 1) = 2 + double n

(* int -> int *)
fun exp 0 = 1
  | exp (n + 1) = double exp n

(* int -> int *)
fun wait 0 = 0
  | wait (n + 1) = wait n
