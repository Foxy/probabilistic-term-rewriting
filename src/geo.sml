(* Generatore pseudocasuale *)
val rand = Random.rand (0, 0);

(* unit -> bool *)
fun coinToss _ = Random.randRange (0, 1) rand = 1;

(* int -> int *)
fun silly 0 = 0
  | silly (n + 1) =
      if coinToss ()
        then silly n
        else n + 1;

(* int -> int *)
fun geo n = if coinToss ()
  then geo (n + 1)
  else n;
