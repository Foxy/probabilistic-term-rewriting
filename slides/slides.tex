\documentclass{beamer}

\usepackage[italian]{babel}
\usepackage{caption}
\usepackage{listings}
\usepackage{tikz}

\usetheme{Pittsburgh}
\usecolortheme{beaver}
\usefonttheme{serif}
\setbeamerfont{caption}{size=\tiny}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{HTML}{999988}
\definecolor{codered}{HTML}{D73A49}
\definecolor{codeblack}{HTML}{212121}

\lstdefinestyle{mystyle}{
	commentstyle=\color{codegray},
	keywordstyle=\color{codered},
        identifierstyle=\color{codeblack},
	numberstyle=\tiny\color{codegray},
	basicstyle=\ttfamily\scriptsize,
	breakatwhitespace=false,
	breaklines=true,
	captionpos=n,
	keepspaces=true,
	numbers=left,
	numbersep=5pt,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	tabsize=2
}

\lstset{style=mystyle}
\renewcommand{\lstlistingname}{Programma}
\renewcommand{\lstlistlistingname}{Indice dei programmi}

\newcommand{\smlinputlisting}[2]{{
	\lstinputlisting[
    language=ML,
    caption={#2},
    escapeinside={LaTeX\{}{\}\}}
  ]{#1}
}}

\newcommand{\sml}[2]{{
	\smlinputlisting{../src/#1}
  {#2 (\texttt{#1})}
}}

\newcommand{\smlinputlistingrange}[3]{{
  \lstinputlisting[
    language=ML,
    caption={#2},
    linerange={#3},
  ]{#1}
}}

\newcommand{\smlrange}[3]{{
  \smlinputlistingrange{../src/#1}{#2 (\texttt{#1}, #3)}{#3}
}}

\setlength\abovecaptionskip{4pt}

\title{\small{Sulla riscrittura dei termini in presenza di effetti probabilistici}}
\author{\tiny{Stefano Volpe}}
\institute{\tiny{Alma Mater Studiorum $\cdot$ Universit\`a di Bologna}}
\date{\tiny{19 luglio 2023} \\~\\ \includegraphics[width=0.1\textwidth]{assets/by-nc-sa-4-0}}
\titlegraphic{
  \begin{figure}
    \includegraphics[height=.4\textheight]{assets/the-gaming-house}
    \caption{Hogarth, William. \emph{The Gaming House}. 1732-1734}
  \end{figure}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Indice}
  \tableofcontents
\end{frame}

\section{Riscrittura dei termini}
\begin{frame}{Riscrittura dei termini (1)}
  Un altro modello di computazione \textbf{Turing-completo}:

  \begin{center}
  \includegraphics[width=0.1\textwidth]{assets/abacus}
  \includegraphics[width=0.1\textwidth]{assets/latex}
  \includegraphics[width=0.1\textwidth]{assets/magic-the-gathering}
  \includegraphics[width=0.1\textwidth]{assets/minecraft}
  \includegraphics[width=0.1\textwidth]{assets/recursive-functions}
  \includegraphics[width=0.1\textwidth]{assets/turing-machine}
  \end{center}

  Semplice e affine a \textbf{computazione simbolica} e \textbf{programmazione funzionale}.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Riscrittura dei termini (2)}
  Regole:
  $$X + 0 \rightarrow X$$
  $$X + S(Y) \rightarrow S(X + Y)$$
  \pause
  Riduzione:
  $$S(0) + S(S(0)) \rightarrow S(S(0) + S(0)) \rightarrow S(S(S(0) + 0)) \rightarrow S(S(S(0)))$$
\end{frame}

\subsection{Terminazione}
\begin{frame}{TRS terminante}
  \begin{block}{Definizione}
	  Un sistema di riscrittura dei termini (TRS) $\rightarrow$ è \textbf{terminante} sse non esiste
	  alcuna catena infinita discendente $a_0 \rightarrow a_1 \rightarrow \dots$.
  \end{block}
\end{frame}

\subsection{Ordini di percorso lessicografici}
\begin{frame}{Ordini di percorso lessicografici (LPO)}
	Assumendo $f > g$, vale che $f(g(x, y)) >_{lpo} g(f(y), f(x))$:
  \begin{center}
  \begin{tikzpicture}
  \node {$f$}
    child {node {$g$}
      child {node {$x$}}
      child {node {$y$}}
    };
  \end{tikzpicture}
  \begin{tikzpicture}
  \node {$g$}
    child {node {$f$}
      child {node {$y$}}}
    child {node {$f$}
      child {node {$x$}}
    };
  \end{tikzpicture}
  \end{center}
  \pause
  \begin{block}{Teorema}
    Ogni LPO è terminante.
  \end{block}
\end{frame}

\section{Riscrittura dei termini e probabilit\`a}
\begin{frame}[fragile]
  \frametitle{Riscrittura dei termini e probabilit\`a}
  Regole:
  \smlrange{geo.sml}{}{8-12}
  \pause
  Riduzione:
  $$\texttt{silly 2} \xrightarrow +
  \{\!\!\{ \frac{1}{2}: \texttt{silly 1}, \frac{1}{2}: \texttt{2}\}\!\!\} \xrightarrow +
  \{\!\!\{ \frac{1}{4}: \texttt{silly 0}, \frac{1}{4}: \texttt{1}, \frac{1}{2}: \texttt{2}\}\!\!\} \xrightarrow +$$
  $$\xrightarrow + \{\!\!\{ \frac{1}{4}: \texttt{0}, \frac{1}{4}: \texttt{1}, \frac{1}{2}: \texttt{2}\}\!\!\}$$
\end{frame}

\subsection{Terminazione quasi certa forte}
\begin{frame}{PTRS quasi certamente terminante forte}
  \begin{block}{Definizione}
	  Un sistema di riscrittura dei termini probabilistico (PTRS) $\rightarrow$ \`e \textbf{quasi
	  certamente terminante forte (SAST)} sse ogni termine ha altezza di derivazione attesa finita.
  \end{block}
\end{frame}

\subsection{Ordini probabilisticamente monotoni}
\begin{frame}[fragile]
  \frametitle{Ordini probabilisticamente monotoni (PMO)}
  Un PMO $>_{p\succ}$ indotto da $\succ$:
  \begin{enumerate}
    \item non si muove mai ``controcorrente'' rispetto a $\succ$;
    \item si muove nello stesso senso di $\succ$ con probabilit\`a $\geq \epsilon$.
  \end{enumerate}
  \smlrange{geo.sml}{}{8-12}
  \pause
  \`e un sottoinsieme del PMO $>_{p\succ}$, con:
  \begin{enumerate}
    \item $\succ =$ ``ha altezza maggiore di'';
    \item $\epsilon = \frac{1}{2}$.
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ordini probabilisticamente monotoni (PMO) via codifica}
  Un PMO $>_{p\succ},f$ indotto da $\succ$ \textbf{via codifica} $f$:
  \begin{enumerate}
    \item \`e sempre \textbf{non crescente rispetto a $f$};
    \item si muove nello stesso senso di $\succ$ con probabilit\`a $\geq \epsilon$.
  \end{enumerate}
  \smlrange{geo.sml}{}{15-17}
  \pause
  \`e un sottoinsieme del PMO \textbf{via codifica} $>_{p\succ,f}$, con:
  \begin{enumerate}
    \item $\succ =$ ``ha pi\`u occorrenze di \texttt{geo} rispetto a'';
    \item $f(s) =$ ``il numero di occorrenze di \texttt{geo} in $s$'';
    \item $\epsilon = \frac{1}{2}$.
  \end{enumerate}
\end{frame}

\subsubsection{PMO e SAST}
\begin{frame}{PMO e SAST}
  \begin{block}{Teorema}
    Ogni PMO (con o senza codifica) \`e SAST.
  \end{block}
\end{frame}

\subsubsection{PMO e LPO}
\begin{frame}{Ordini di percorso lessicografici (LPO)}
	Assumendo $f > g$, vale che $f(g(x, y)) >_{lpo} g(f(y), f(x))$:
  \begin{center}
  \begin{tikzpicture}
  \node {$f$}
    child {node {$g$}
      child {node {$x$}}
      child {node {$y$}}
    };
  \end{tikzpicture}
  \begin{tikzpicture}
  \node {$g$}
    child {node {$f$}
      child {node {$y$}}}
    child {node {$f$}
      child {node {$x$}}
    };
  \end{tikzpicture}
  \end{center}
  \pause
  \begin{block}{Teorema}
    Esistono LPO che non inducono PMO (n\'e con n\'e senza codifica).
  \end{block}
\end{frame}

\begin{frame}{Grazie!}
\end{frame}

\end{document}
