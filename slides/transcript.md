# Trascrizione della spiegazione sulle diapositive

## Diapositiva 1

Buongiorno, sono Stefano Volpe. L'oggetto della mia tesi sono i sistemi di
riscrittura, un modello molto utilizzato nello studio dei linguaggi di
programmazione. Nella fattispecie, cerchiamo soluzioni al problema della
terminazione nel caso probabilistico.

## Diapositiva 2

In questa presentazione, prima ripercorriamo la riscrittura dei termini
``tradizionale'' e il relativo problema della terminazione. In seguito,
ne presentiamo l'estensione probabilistica e i risultati originali da me
proposti. Essi hanno a che fare con i cosiddetti ordini di percorso.

## Diapositiva 3

La riscrittura dei termini è un modello di computazione Turing-completo.
L'interesse nei suoi confronti deriva dalla sua semplicità e dall'affinità con
la computazione simbolica. La computazione viene vista come un processo di
sostituzione di espressioni con espressione, senza prevedere esplicitamente
concetti quali variabili e stati.

## Diapositiva 4

In questo esempio tratto dall'aritmetica, definiamo il nostro sistema di
riscrittura tramite due regole. "S" indica la funzione successore.

## Diapositiva 5

Il nostro sistema può adesso computare arbitrarie somme fra due o più numeri
naturali. In questo esempio, la somma fra 1 e 2 è valutata come 3.

## Diapositiva 6

Banalmente, la terminazione è definita come inesistenza di catene di riduzione
infinite. Conseguenza della Turing-completezza e della non decidibilità del
problema della fermata è che non esistano algoritmi in grado di decidere se un
sistema di riscrittura sia terminante o meno.

## Diapositiva 7

Vogliamo quindi procedure per costruire specifiche famiglie di sistemi la cui
terminazione sia garantita. Eccone un esempio: gli ordini di percorso
lessicografici, che confrontano dapprima i simboli alla radice dei due termini e
poi, in caso di parità, ricorsivamente, i loro figli immediati.

## Diapositiva 8

Kami e Lévin dimostrano che ogni LPO è terminante. Esistono in realtà    

## Diapositiva 9

Il nostro esempio probabilistico, invece, è tratto dalla programmazione
funzionale. Questa procedura, nel caso ricorsivo, lancia una moneta: se esce
testa, valuta la chiamata ricorsiva sull'antecedente; altrimenti, restituisce
l'argomento. Le regole di riscrittura, stavolta non sono state esplicitate, ma
si desumono dal codice.

## Diapositiva 10

Le riduzioni probabilistiche lavorano con sottomultidistribuzioni per esprimere
tutti i possibili esiti con le relative probabilità. In questo esempio, la
chiamata in questione viene ridotta. Abbiamo un quarto di probabilità che esca
0,un quarto che esca 1 e un mezzo che esca 2.

## Diapositiva 11

Esistono più nozioni di terminazione probabilistica, poiché la condizione di
inesistenza di catene di riduzione infinite rischia di essere troppo
restrittiva. Fra le varie definizioni da noi analizzate, la più stringente
risulta essere SAST. Essa impone la finitezza dell'altezza attesa dell'albero di
riduzione di ogni termine.

## Diapositiva 12

Ovviamente, il problema della fermata non è decidibile neanche quando formulato
per i sistemi di riscrittura probabilistici (per nessuna delle definizioni di
terminazione probabilistica comunemente accettate). Il mio contributo originale
consiste nell'introduzione di una nuova famiglia di sistemi di riscrittura
probabilistica: quelli fondati su ordini che ho chiamato "probabilisticamente
monotoni". Essi sono indotti da normali ordini non probabilistici e seguono
due regole:

- non si muovono mai "controcorrente" rispetto all'ordine non probabilistico di
  partenza;
- a ogni passo di computazione, c'è almeno una probabilità non nulla che generi
  un successore coerente con l'ordine di partenza.

## Diapositiva 13

Il programma `silly`, quindi, rispetta le regole dell'ordine probabilisticamente
monotono indotto dall'ordine "ha altezza maggiore di":

- nessun ramo di computazione si muove "controcorrente" rispetto all'ordine
  "avere altezza maggiore di";
- ogni ramo di computazione genera un successore coerente con l'ordine "avere
  altezza maggiore di".

## Diapositiva 14

Alcuni programmi richiedono una formulazione diversa di ordine
probabilisticamente monotono, che chiama in causa anche una codifica dei termini
verso i numeri reali non negativi. In questo caso, cambia solo il primo vincolo:
l'ordine probabilisticamente monotono è sempre non crescente rispetto a f.
Questo programma, nel ramo di computazione induttiva lancia una moneta: se esce
testa, effettua una chiamata ricorsiva sul successore dell'argomento; se esce
croce, restituisce l'argomento.

## Diapositiva 15

Quindi basta scegliere come ordine di partenza "ha più occorrenze di `geo`
rispetto a", e come codifica "il numero di occorrenze di `geo` rispetto a":
- ogni ramo di computazione non aumenta il numero di occorrenza di `geo`;
- il secondo ramo, addirittura, le diminuisce, generano un successore coerente
  con l'ordine "ha più occorrenze di `geo` rispetto a".

## Diapositiva 16

Il mio contribuito non si limita alla definzione di questi due nuovi concetti
inediti, ma anche al provare che essi siano SAST. Essendo questa la condizione
di terminazione probabilistica più forte fra quelle analizzate, tutte le altre
seguono banalmente.

## Diapositiva 17

Concludo enunciando anche un risultato negativo, sempre da me dimostrato nella
conclusione del mio lavoro. Abbiamo detto che gli ordini di percorso
lessicografici sono sempre terminanti, e quindi potremmo avere la tentazione di
indurvi ordini probabilisticamente monotoni.

## Diapositiva 18

Ho dimostrato però che nessuna delle mie due definizioni di ordine
probabilisticamente monotono accetta ordini di percorso lessicografici come
ordini di partenza.

## Diapositiva 19

Grazie per l'attenzione! Ho concluso e accetto domande.
