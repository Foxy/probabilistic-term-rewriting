\chapter{Prerequisiti}

Questo capitolo introduttivo pone essenziali fondamenta per la trattazione
che gli seguirà. Una volta chiarito il contesto disciplinare in cui la
riscrittura probabilistica dei termini si colloca, vengono rievocati in modo
particolarmente rigoroso concetti preliminari di algebra universale e teoria
delle probabilità.

\section{Introduzione informale}

\subsection{La riscrittura dei termini}

La riscrittura dei termini, fondata sulla logica equazionale, affianca
alle identità di quest'ultima delle regole di riscrittura direzionate. Non si
limita quindi a esprimere il fatto che un termine $t$ sia equivalente a un
termine $u$, ma può anche rappresentare la nozione che $t$ sia ``riscrivibile''
in un passo di computazione come $u$. A cavallo fra l'informatica teorica,
l'algebra universale, la dimostrazione automatica di teoremi e la programmazione
funzionale, la riscrittura dei termini è interessata ai sistemi di calcolo
convergenti (anche se potenzialmente non-deterministici) e alle tecniche per
individuarli.

Anche se loro predecessori erano già noti agli inizi del XX secolo,
storicamente, i sistemi di riscrittura dei termini ``propriamente detti'' vedono
la luce con i lavori di Evans \cite{evans} (1951) prima, e di Knuth e Bendix
\cite{knuth} (1970) poi. Inizialmente, lo scopo inteso era quello di produrre
sistemi di riscrittura di termini cosiddetti ``canonici'', con lo scopo di
provare uguaglianze in determinate teorie equazionali. Verso la fine degli anni
70, l'emergere dei sistemi equazionali come strumenti per specificare tipi di
dato astratto avrebbe ravvivato l'entusiasmo per la disciplina.

In questa esposizione rimarremo, per quanto possibile, fedeli al modello di
riscrittura dei termini delineato da Baader e Nipkow \cite{baader}. Per
applicarlo a un sistema di calcolo è sufficiente riconoscere in esso:

\begin{itemize}
  \item una segnatura, e cioè una funzione che attribuisca un'arietà a ogni
    simbolo di funzione;
  \item un sistema di riscrittura probabilistico, e cioè un insieme di regole
    di riscrittura (opportunamente generalizzate tramite uso di variabili) che
    esprimano i possibili passi di computazione.
\end{itemize}

Nella nostra esposizione (dedicata invece al caso probabilistico), avremo cura
di formalizzare i vincoli che questi oggetti devono rispettare.

\begin{example}[Somme fra numeri naturali]
  Se nel nostro modello vogliamo considerare solo numeri naturali e somme
  fra essi, una possibile segnatura $\Sigma$ potrebbe essere:
  $$\Sigma(0) = 0$$
  $$\Sigma(S) = 1$$
  $$\Sigma(+) = 2$$
  Qui, $S$ è la funzione successore. Si noti come in algebra universale i
  simboli di costante siano semplicemente visti come simboli di funzione 0-ari.
\end{example}

\begin{example}[Somme fra numeri naturali (continua)]
  Si considerino i termini costruiti in base alla segnatura dell'esempio di cui
  sopra. Usando $X$ e $Y$ come variabili che spaziano sull'insieme di tali
  termini, possiamo specificare le due regole che permettono di  valutarli come
  espressioni aritmetiche:

  $$X + 0 \rightarrow X$$
  $$X + S(Y) \rightarrow S(X + Y)$$

  Queste regole sono orientate: affinché la nostra computazione progredisca,
  esse indicano che devono essere applicate da sinistra verso destra.
\end{example}

\subsection{Linguaggi funzionali probabilistici}

I modelli di calcolo probabilistici non sono solo una bizzarria teorica, ma
presentano utili applicazioni pratiche, che spaziano dagli algoritmi casuali
alla crittografia. Nel caso di un generico linguaggio funzionale universale, un
modo semplice di realizzare un'estensione probabilistica anch'essa universale è
aggiungere un operatore di campionamento su una distribuzione di Bernoulli equa
(e cioè quella del lancio di una moneta non truccata).

\begin{example}[\emph{Quicksort} casuale]
  Il \emph{Quicksort} propriamente detto ha, nel caso peggiore, costo temporale
  asintotico $\Theta(n^2)$. La sua variante casuale ha costo temporale
  asintotico atteso $\Theta(n\ log(n))$. La nostra esposizione sarà costellata
  di diversi esempi in Standard ML \cite{sml}, tutti focalizzati sulla
  semplicità anziché su prestazioni ottimali.
\end{example}

\sml{rq-sort.sml}{\emph{Quicksort} casuale}

Ai fini del nostro studio, Standard ML non fungerà solo da linguaggio-oggetto in
cui esprimere programmi probabilistici di cui provare proprietà utili: lo
useremo anche come meta-linguaggio per formalizzare gli stessi concetti esposti,
sulla traccia di Baader e Nipkow \cite{baader}, da cui riprenderemo anche
alcuni frammenti di implementazione. Tutti i materiali sono disponibili presso:

\url{https://codeberg.org/Foxy/probabilistic-lexicographic-path-orders}

\subsection{La riscrittura probabilistica dei termini}

Il modello di riscrittura probabilistica dei termini su cui questo studio si
basa è sostanzialmente analogo a quello presentato da Avanzini et al.\
\cite{avanzini}, a sua volta ispirato a Bournez e Garnier \cite{bournez}, in
contrasto a quello di Agha et al.\ \cite{agha}. Lo svolgersi della riscrittura è
concepito come un processo decisionale di Markov. Se, da un lato, a ogni passo
si ha una nondeterministica selezione della seguente applicazione corretta di
regola, dall'altro lo stesso fatto che ciascuna regola possa avere più esiti
possibili contribuisce con un proprio grado di incertezza.

\begin{example}[Passeggiata aleatoria]
  Si consideri la seguente segnatura per termini che rappresentano numeri
  naturali: 
  $$\Sigma(0) = 0$$
  $$\Sigma(S) = 1$$
  A partire da essa, usando $X$ come variabile per un qualsiasi termine basato
  su di essa, possiamo specificare una regola di riscrittura che trasforma un
  numero nel suo successore con $\frac{1}{2}$ di probabilità, e nel suo
  precedente con il restante $\frac{1}{2}$ di probabilità:
  $$S(X) \rightarrow \{\!\!\{\frac{1}{2} : X; \frac{1}{2} : S(S(X))\}\!\!\}$$
  Questa regola di riscrittura, anche se presa singolarmente, forma già un
  corretto sistema probabilistico di riscrittura dei termini.
\end{example}

\section{Concetti preliminari}

\subsection{Algebra universale}

\subsubsection{Termini}

Come da consueto, i termini con cui lavoreremo saranno costruiti usando simboli
di funzione e variabili. Per rappresentare l'arietà di ciascun simbolo di
funzione, introduciamo il concetto di segnatura.

\begin{definition}[Segnatura]
  Sia $\Phi$ un insieme di simboli di funzione. Una segnatura per $\Phi$ è una
  qualsiasi funzione
  $$\Sigma_\Phi \quad : \quad \Phi \rightarrow \mathbb{N}$$
\end{definition}

Intuitivamente, una segnatura associa a ciascun simbolo di funzione la sua
arietà. Si noti che nella nostra trattazione l'insieme dei numeri naturali
$\mathbb{N}$ include sempre $0$. Possiamo inoltre partizionare i simboli di
funzione in base alla loro arietà.

\begin{definition}[Insieme degli elementi n-ari]
  Siano $\Sigma_\Phi$ una segnatura per $\Phi$ e $n \in \mathbb{N}$. L'insieme
  $\Sigma_\Phi^{(n)}$ di tutti gli elementi n-ari di $\Phi$ è definito come
  segue:
  $$\Sigma_\Phi^{(n)} := \{f \in \Phi \quad | \quad \Sigma_\Phi(f) = n\}$$
\end{definition}

I simboli di costante sono chiaramente già contemplati nella definizione
precedente:

\begin{definition}[Simbolo di costante]
  Sia $\Sigma_\Phi$ una segnatura per $\Phi$. Un simbolo di funzione
  $e \in \Phi$ si dice essere un simbolo di costante rispetto a $\Sigma_\Phi$ se
  e solo se
  $$e \in \Sigma_\Phi^{(0)}$$
\end{definition}

Possiamo ora procedere a combinare i simboli di funzione con le variabili per
formare termini. Per evitare ambiguità, imponiamo che l'insieme dei simboli di
funzione e quello delle variabili siano distinti.

\begin{definition}[Insieme dei termini]
  Siano $\Sigma_\Phi$ una segnatura e $X$ un insieme di variabili tali che
  $\Phi \cap X = \varnothing$. Definiamo per induzione $T(\Sigma_\Phi, X)$,
  l'insieme dei $\Sigma_\Phi$-termini su $X$:
  \begin{enumerate}
    \item $X \subseteq T(\Sigma_\Phi, X)$;
    \item $\forall n \in \mathbb{N} \quad \forall f \in \Sigma_\Phi^{(n)} \quad \forall t_1, \dots, t_n \in T(\Sigma_\Phi, X) \quad f(t_1, \dots, t_n) \in T(\Sigma_\Phi, X)$;
    \item nient'altro è un $\Sigma_\Phi$-termine su $X$.
  \end{enumerate}
\end{definition}

Con la clausola base, abbiamo reso ogni variabile un termine; con quella
induttiva, abbiamo reso ogni applicazione di un simbolo di funzione a dei
termini (dove la arietà del simbolo è rispettata) essa stessa un termine. Ora
che abbiamo specificato il rapporto di disgiunzione fra l'insieme dei simboli
di funzione e quello delle variabili, non abbiamo più interesse a esplicitare
pedantemente il primo ogni volta. Da qui in poi, useremo $\Sigma$ per riferirci
a una segnatura per un qualche insieme di simboli di funzione.

Baader e Nipkow \cite{baader}, per esempio, partono da questa definizione per
costruire il loro tipo \texttt{term} in Standard ML:

\smlrange{traat/trs.ML}{termini di un generico TRS}{14-16}

La definzione per induzione appena data può essere vista come una grammatica
disambigua per i termini che ci permette di costruire da essi degli alberi di
sintassi astratta. Inoltre, dato un termine, possiamo identificare univocamente
una qualsiasi posizione all'interno del suo albero di sintassi astratta
specificando l'unico cammino discendente che ci permette di raggiungerla a
partire dalla radice.

Nella definizione che segue, usiamo $\mathbb{N}^*$ per indicare l'insieme di tutte le
possibili stringhe costruite su $\mathbb{N}$, ed $\epsilon$ per indicare la
stringa vuota.

\begin{definition}[Insieme delle posizioni]
  Siano $T(\Sigma, X)$ l'insieme dei $\Sigma$-termini su $X$ e $s \in T(\Sigma, X)$.
  Definiamo per induzione $\mathcal{P}\mathit{os}(s) \subseteq{\mathbb{N}^*}$, l'insieme
  delle posizioni del termine $s$:
  \\~\\
  \begin{math}
  \mathcal{P}\mathit{os}(s) =
    \begin{cases}
      \text{$\{\epsilon\}$} &\quad\text{se $s \in X$} \\
      \text{$\{\epsilon\} \cup \bigcup_{i = 1}^n \{ip \mathbin{|} p \in \mathcal{P}\mathit{os}(s_i)\}$} &\quad\text{se $s = f(s_1, \dots, s_n)$} \\
    \end{cases}
  \end{math}
  \\~\\
\end{definition}

Nella definizione di cui sopra, ciascun cammino è codificato come una stringa:
se $n$ è l'i-esimo passo di una posizione, allora il l'i-esimo passo del cammino
ad essa associato ha come destinazione il figlio di indice $n$ del nodo
corrente. L'insieme delle posizioni ci dà inoltre un'idea della ``misura'' di un
dato termine.

\begin{definition}[Dimensione di un termine]
  Siano $T(\Sigma, X)$ l'insieme dei $\Sigma$-termini su $X$ e $s \in T(\Sigma, X)$.
  La dimensione del termine $s$ è:
  $$|s| \quad := \quad |\mathcal{P}\mathit{os}(s)|$$

\end{definition}


Avendo le posizioni, possiamo ora estrarre da un termine i suoi sottotermini.

\begin{definition}[Sottotermine]
  Siano $T(\Sigma, X)$ l'insieme dei $\Sigma$-termini su $X$, $s \in T(\Sigma, X)$
  e $p \in \mathcal{P}\mathit{os}(s)$. Per induzione su $p$ definiamo $s|_p$, il
  sottotermine di $s$ in posizione $p$:
  \\~\\
  \begin{math}
    s|_p =
    \begin{cases}
      \text{$s$} &\quad\text{se $p = \epsilon$} \\
      \text{$s_i|_q$} &\quad\text{se $p = iq$ (e quindi $s = f(s_1, \dots, s_n)$)} \\
    \end{cases}
  \end{math}
\end{definition} 

\subsubsection{Sostituzioni e contesti}

Un termine può essere modificato rimpiazzando uno dei suoi sottotermini con un
altro.

\begin{definition}[Termine ottenuto per rimpiazzamento]
  Siano $T(\Sigma, X)$ l'insieme dei $\Sigma$-termini su $X$, $s,t \in T(\Sigma, X)$
  e $p \in \mathcal{P}\mathit{os}(s)$. Per induzione su $p$ definiamo
  $s[t]_p$, il termine ottenuto da $s$ rimpiazzando il suo sottotermine in
  posizione $p$ con il termine $t$:
  \\~\\
  \begin{math}
    s[t]_p =
    \begin{cases}
      \text{$t$} &\quad\text{se $p = \epsilon$} \\
      \text{$f(s_1, \dots, s_i[t]|_q, \dots, s_n)$} &\quad\text{se $p = iq$ (e quindi $s = f(s_1, \dots, s_n)$)} \\
    \end{cases}
  \end{math}
\end{definition} 

Molto spesso, saremo interessati a rimpiazzare le variabili presenti in un dato
termine, e quindi a sapere quali di esse effettivamente occorrano in esso.

\begin{definition}[Insieme delle variabili occorrenti in un termine]
  Siano $T(\Sigma, X)$ l'insieme dei $\Sigma$-termini su $X$ e
  $s \in T(\Sigma, X)$. L'insieme delle variabili occorrenti in $s$ è:
  $$\mathcal{V}\mathit{ar}(s) := \{ x \in X \mathbin{|} \exists p \in \mathcal{P}\mathit{os}(s) \quad s|_p = x\}$$
\end{definition}

Sempre con l'intento di vincolarci al rimpiazzamento di termini che siano anche
variabili, sarà importante sapere se un sottotermine in una posizione data sia
effettivamente una variabile o meno.

\begin{definition}[Posizione di variabile]
  Siano $T(\Sigma, X)$ l'insieme dei $\Sigma$-termini su $X$, $s \in T(\Sigma, X)$
  e $p \in \mathcal{P}\mathit{os}(s)$. $p$ è una posizione di variabile se e
  solo se
  $$s|_p \in X$$
\end{definition}

Le sostituzioni permettono di rimpiazzare uniformemente un qualsiasi numero
finito di variabili all'interno di un termine. Nel seguito, costruiremo i nostri
termini su insiemi di variabili sì infiniti, ma numerabili, e cioè di
cardinalità $\aleph_0$.

\begin{definition}[Sostituzione di variabili]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$. Una $T(\Sigma, V)$-sostituzione di
  variabili $\sigma$ è una qualsiasi funzione
  $$\sigma : V \rightarrow T(\Sigma, V) \quad , \quad |\{x \in V \mathbin{|} x \not = \sigma(x)\}| < \aleph_0$$
\end{definition}

Le variabili che una sostituzione non mappa in sé stesse (e sulle quali quindi
la sostituzione di variabili effettuamente agisce) costituiscono il dominio di
detta sostituzione.

\begin{definition}[Dominio di una sostituzione di variabili]
  Sia $\sigma$ una $T(\Sigma, V)$-sostituzione di variabili. Il dominio di
  $\sigma$, $\mathcal{D}\mathit{om}(\sigma)$, è
  $$\mathcal{D}\mathit{om}(\sigma) := \{x \in V \mathbin{|} x \not = \sigma(x)\}$$
\end{definition}

Il concetto di sostituzione di variabili è facilmente estendibile ai termini.

\begin{definition}[Sostituzione per termini]
  Sia $\sigma$ una $T(\Sigma, X)$-sostituzione di variabili. Per induzione su
  $t \in T(\Sigma, X)$ definiamo $\hat{\sigma}$, la $T(\Sigma, X)$-sostituzione
  per termini basata su $\sigma$:
  \\~\\
  \begin{math}
    \hat{\sigma}(s) =
    \begin{cases}
      \text{$\sigma(s)$} &\quad\text{se $s \in V$} \\
      \text{$f(\sigma(s_1), \dots, \sigma(s_n))$} &\quad\text{se $s = f(s_1, \dots, s_n)$} \\
    \end{cases}
  \end{math}
\end{definition}

Quando parliamo di ``dominio di una sostituzione di termini'', ovviamente
facciamo riferimento al dominio della sostituzione di variabili sottostante.

\begin{definition}[Dominio di una sostituzione di termini]
  Siano $\sigma$ una $T(\Sigma, X)$-sostituzione di variabili e $\hat{\sigma}$
  la $T(\Sigma, X)$-sostituzione di termini basata su $\sigma$. Il dominio di
  $\hat{\sigma}$, $\mathcal{D}\mathit{om}(\hat{\sigma})$, è:
  $$\mathcal{D}\mathit{om}(\hat{\sigma}) := \mathcal{D}\mathit{om}(\sigma)$$
\end{definition}

Spesso, tornerà utile considerare l'insieme di tutte le possibili
$(\Sigma, V)$-sostituzioni di termini.

\begin{definition}[Insieme delle sostituzioni di termini]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$. L'insieme delle
  $(\Sigma, V)$-sostituzioni di termini,
  $\mathcal{S}\mathit{ub}(T(\Sigma, V))$, è:
  $$\mathcal{S}\mathit{ub}(T(\Sigma, V)) := \{\hat{\sigma} : T(\Sigma, V) \rightarrow T(\Sigma, V) \mathbin{|} \hat{\sigma} \ \text{è una} \ (\Sigma, V)\text{-sostituzione di termini}\}$$
\end{definition}

Quando sarà chiaro dal contesto, potremmo limitarci a riferirci alla
sostituzioni di termini come ``sostituzioni'', e evitare di denominarle facendo
uso di accenti circonflessi.

L'ultimo concetto di algebra universale di cui necessitiamo è quello di
contesto.

\begin{definition}[Contesto]
  Sia $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $\Box \not\in V$.
  Un contesto $C$ è un qualsiasi $\Sigma$-termine su $V \cup \{\Box\}$ tale che
  $$|\{ p \in \mathcal{P}\mathit{os}(C) \mathbin{|} C|_p = \Box \} = 1$$
\end{definition}

\begin{definition}[Popolazione di un contesto]
  Sia $C$ un contesto per $\Sigma$-termini su $V$ in cui $\Box$ occorre in
  posizione $p$ e sia $s$ un $\Sigma$-termine su $V$. La popolazione del
  contesto $C$ con $s$, $C[s]$, è definita come
  $$C[s] := C[s]_p$$
\end{definition}

Un contesto funge insomma da termine contenente un singolo ``buco'' $\Box$,
pronto a essere riempito tramite sostituzione. Avremo modo di fare uso
dell'insieme di tutti i contesti per $\Sigma$-termini su $V$.

\begin{definition}[Insieme dei contesti]
  Siano $V$ un insieme di variabili tale che $|V| = \aleph_0$ e $T(\Sigma, V)$
  l'insieme dei $\Sigma$-termini su $V$, con $\Box \not\in V$. L'insieme dei
  contesti per $\Sigma$-termini su $V$, $\mathcal{C}\mathit{on}(T(\Sigma, V))$, è:
  $$\mathcal{C}\mathit{on}(T(\Sigma, V)) := \{C \in T(\Sigma, V \cup \{\Box\})
  \mathbin{|} \text{$C$ è un contesto}\}$$
\end{definition}

\subsection{Probabilità}

I prerequisiti di probabilità di cui necessitiamo poggiano sul concetto di
multinsieme finito.

\begin{definition}[Multinsieme finito]
  Sia $A$ un insieme finito. Un multinsieme finito su $A$ è una qualsiasi
  funzione
  $$M : A \rightarrow \mathbb{N}$$
\end{definition}

D'ora in poi, quando parleremo di multinsiemi, ci concederemo la libertà di
sottointenderne la finitezza. Intuitivamente, un multinsieme mappa ogni suo
elemento nel numero di ``copie'' di tale elemento contenute nel multinsieme
stesso. Possiamo unire una quantità finita di multinsiemi sullo stesso insieme
di partenza semplicemente sommando, per ciascun elemento, i rispettivi numeri di
copie.

\begin{definition}[Unione di un numero finito di multinsiemi finiti]\label{def:multiset-union}
  Siano $A$ un insieme finito e $I$ un insieme finito tale che
  $$\forall i \in I \quad M_i \ \text{è un multinsieme finito su $A$}$$
  In questo caso:
  $$\forall a \in A \quad (\biguplus_{i \in I}M_i)(a) := \sum_{i \in I}M_i(a)$$
\end{definition}

Anche quando sommiamo tutti gli elementi di un multinsieme dobbiamo tener conto
dei numeri delle copie di ciascuno di essi.

\begin{definition}[Somma di un multinsieme finito rispetto a una funzione]
  Sia $M$ un multinsieme finito su $A$ e sia $f : A \rightarrow \mathbb{R}$. La
  somma di $M$ rispetto a $f$ è
  $$\sum_{a \in M} f(a) := \sum_{a \in A} M(a) f(a)$$
\end{definition}

Un multinsieme di facile definizione è quello vuoto, che non contiene alcuna
copia di alcun elemento.

\begin{definition}[Multinsieme vuoto]
  Sia $A$ un insieme finito. Il multinsieme vuoto su $A$, $\varnothing_A$, è
  definito come
  $$\forall a \in A \quad \varnothing_A(a) := 0$$
\end{definition}

Possiamo costruire multinsiemi anche in altri modi, per esempio partendo da un
insieme.

\begin{definition}[Multinsieme finito da un insieme finito di indici]\label{def:multiset-from-indices}
  Siano $A$ e $I$ insiemi finiti tali che
  $$\forall i \in I \quad a_i \in A$$
  In questo caso, $M = \{\!\!\{a_i \mathbin{|} i \in I \}\!\!\}$, è il
  multinsieme finito su $A$ tale che:
  $$\forall a \in A \quad M(a) := |\{ i \in I \mathbin{|} a_i = a\}|$$
\end{definition}

In Standard ML, un modo pigro di rappresentare un multinsieme è tramite una
lista che ne contiene tutte le ipotetiche copie nel giusto numero e senza un
ordine prestabilito:

\smlrange{multidistributions.sml}{multinsiemi come liste}{1-2}

Nel caso finito dei numeri naturali da $1$ a $n$ inclusi, possiamo anche
orientarci verso una scrittura più sintetica.

\begin{definition}[Multinsieme finito da un insieme finito di indici]
  Siano $A$ un insieme finito e $n \in \mathbb{N}$ tali che
  $$\forall i \in \{1, \dots, n\} \quad a_i \in A$$
  In questo caso:
  $$\{\!\!\{a_1, ..., a_n\}\!\!\} := \{\!\!\{a_i \mathbin{|} i \in \{1, \dots, n\}\}\!\!\}$$
\end{definition}

Così rappresentati, i multinsiemi si prestano molto più facilmente
all'operazione di unione vista sopra.

\begin{theorem}[Formula dell'unione di un numero finito di multinsiemi finiti]\label{thm:multiset-union-formula}
  Siano $I$ e $A$ due insieme finiti tali che
  $$\forall i \in I \quad \{\!\!\{a_{i,j} \mathbin{|} j \in J_i \}\!\!\} \in \mathbb{N}^A$$
  Allora,
  $$\biguplus_{i \in I}\{\!\!\{a_{i,j} \mathbin{|} j \in J_i \}\!\!\} = \{\!\!\{a_{i,j} \mathbin{|} i \in I \wedge j \in J_i \}\!\!\}$$
\end{theorem}

\begin{proof}
  Ovvio per \cref{def:multiset-union} e \cref{def:multiset-from-indices}.
\end{proof}

Ci apprestiamo ora a introdurre il concetto di sottomultidistribuzione, che per
brevità abbrevieremo come ``multidistribuzione''. Un esempio di zucchero
sintattico che ci concederemo sarà quello di usare $p : a$ per indicare coppie
ordinate $(p, a)$, con la connotazione intesa di usare $p$ come una probabilità
e $a$ come un esito.

\begin{definition}[(Sotto)multidistribuzione]\label{def:multidistribution}
  Una (sotto)multidistribuzione $\mu$ su un insieme $A$ è un multinsieme su
  $[0, 1] \times A$ tale che:
  $$\sum_{p:a \in \mu} p \leq 1$$ 
\end{definition}

\smlrange{multidistributions.sml}{multidistribuzioni}{4-5}

Volendo assegnare un nome alla quantità vincolata dalla precedente definizione,
potremmo chiamarla ``probabilità totale'' della multidistribuzione in questione.

\begin{definition}[Probabilità totale di una multidistribuzione]
  La probabilità totale di una multidistribuzione $\mu$ è definita come
  $$|\mu| := \sum_{p:a \in \mu}p$$
\end{definition}

Il calcolo della probabilità totale di una data multidistribuzione in Standard
ML è molto semplice.

\smlrange{multidistributions.sml}{probabilità totale}{7-11}

Definiamo l'insieme di tutte le multidistribuzioni su un dato insieme.

\begin{definition}[Insieme delle multidistribuzioni]
  L'insieme delle multidistribuzioni su un dato insieme $A$,
  $\mathcal{M}_{\leq 1}(A)$, è
  $$\mathcal{M}_{\leq 1}(A) := \{ M : [0, 1] \times A \rightarrow \mathbb{N} \mathbin{|} M \ \text{è una multidistribuzione su} \  A \}$$
\end{definition}

Se una funzione è applicabile agli elementi di un dato insieme, potremmo volerla
applicare simultaneamente a un intera multidistribuzione su tale insieme.

\begin{definition}[Sollevamento di una funzione alle multidistribuzioni]\label{def:lifting-a-function-to-multidistribution}
  Sia $f: A \rightarrow B$. Definiamo il sollevamento di $f$ alle
  multidistribuzioni come l'omonima funzione
  $f: \mathcal{M}_{\leq 1}(A) \rightarrow \mathcal{M}_{\leq 1}(B)$ tale che,
  se $I$ è un insieme tale che $|I| \leq \aleph_0$ e
  $$\forall i \in I \quad p_i : a_i \in [0, 1] \times A$$
  allora:
  $$f(\{\{p_i : a_i \mathbin{|} i \in I\}\}) := \{\{p_i : f(a_i) \mathbin{|} i \in I\}\}$$
\end{definition}

Le multidistribuzioni la cui probabilità totale raggiunge il limite superiore di
cui sopra sono dette ``proprie''.

\begin{definition}[Multidistribuzione propria]
  Una multidistribuzione $\mu$ si dice ``propria'' se e solo se
  $$|\mu| = 1$$
\end{definition}

Come sopra, definiamo ora l'insieme di tutte le multidistribuzioni proprie su un
dato insieme.

\begin{definition}[Insieme delle multidistribuzioni proprie]
  L'insieme delle multidistribuzioni proprie su un dato insieme $A$,
  $\mathcal{M}(A)$, è
  $$\mathcal{M}(A) := \{ M : [0, 1] \times A \rightarrow \mathbb{N} \mathbin{|} M \ \text{è una multidistribuzione propria su} \  A \}$$
\end{definition}

Una multidistribuzione è facilmente moltiplicabile per uno scalare: è
sufficiente moltiplicare per lo scalare in questione tutte le probabilità
coinvolte.

\begin{definition}[Moltiplicazione scalare di una multidistribuzione]
  Sia $\{\!\!\{q_i : a_i \mathbin{|} i \in I \}\!\!\}$ una multidistrubuzione su
  $A$ e sia $p \in \mathbb{R}$. La moltiplicazione scalare di
  $\{\!\!\{q_i : a_i \mathbin{|} i \in I \}\!\!\}$ per $p$ è definita come:
  $$p \cdot \{\!\!\{q_i : a_i \mathbin{|} i \in I \}\!\!\} :=
  \{\!\!\{p \cdot q_i : a_i \mathbin{|} i \in I \}\!\!\}$$
\end{definition}

Prevedere la probabilità totale di una combinazione lineare di
multidistribuzioni è semplice.

\begin{theorem}[Probabilità totale di una combinazione lineare di
  multidistribuzioni]\label{thm:total-probability-of-multidistributions-linear-combination}
  Siano $I$ e $A$ insiemi finiti tali che
  $$\forall i \in I \quad (p_i \in \mathbb{R} \wedge \mu_i \in \mathcal{M}_{\leq 1}(A))$$
  Allora, vale la seguente uguaglianza:
  $$|\biguplus_{i \in I} p_i \cdot \mu_i| = \sum_{i \in I} p_i \cdot |\mu_i|$$
\end{theorem}

\begin{proof}
  Per ogni $i \in I$, sia $\mu_i = \{\!\!\{q_{i,j} : a_{i,j} \mathbin{|} i \in I \}\!\!\}$.
  Usando il \cref{thm:multiset-union-formula}, si ha:
  $$|\biguplus_{i \in I} p_i \cdot \mu_i| =
  |\biguplus_{i \in I} p_i \cdot \{\!\!\{q_{i,j} : a_{i,j} \mathbin{|} i \in I \}\!\!\}| =
  |\biguplus_{i \in I} \{\!\!\{p_i \cdot q_{i,j} : a_{i,j} \mathbin{|} i \in I \}\!\!\}| =$$
  $$
  = |\{\!\!\{p_i \cdot q_{i,j} : a_{i,j} \mathbin{|} i \in I \wedge j \in J_i \}\!\!\}| =
  \sum_{i \in I} \sum_{j \in J_i} p_i \cdot q_j =
  \sum_{i \in I} p_i \cdot \sum_{j \in J_i} q_j =$$
  $$= \sum_{i \in I} p_i \cdot |\mu_i|$$
\end{proof}

Come intuibile, possiamo dimostrare che l'insieme delle multidistribuzioni
su un dato insieme sia chiuso rispetto alle combinazioni sottoconvesse.

\begin{corollary}[Chiusura delle multidistribuzioni rispetto alle combinazioni
  sottoconvesse]\label{cor:multidistributions-are-closed-under-subconvex-conbinations}
  Siano $I$ e $A$ insiemi finiti tali che
  $$(\forall i \in I \quad (p_i \in \mathbb{R} \wedge \mu_i \in \mathcal{M}_{\leq 1}(A))) \wedge
  \sum_{i \in I} p_i \leq 1$$
  Allora, 
  $$\biguplus_{i \in I} p_i \cdot \mu_i \in \mathcal{M}_{\leq 1}(A)$$
\end{corollary}

\begin{proof}
  Per la \cref{def:multidistribution}, mi riduco a dimostrare
  $$|\biguplus_{i \in I} p_i \cdot \mu_i| \leq 1$$
  che, per il
  \cref{thm:total-probability-of-multidistributions-linear-combination}, è
  equivalente a
  $$\sum_{i \in I} p_i \cdot |\mu_i| \leq 1$$
  Siccome sempre per la \cref{def:multidistribution} si dà il caso che
  $$\forall i \in I |\mu_i| \leq 1$$
  il membro di sinistra è maggiorato da $\sum_{i \in I} p_i$, che è minore o
  uguale a $1$ per ipotesi. Anche la disuguaglianza originale è quindi
  dimostrata.
\end{proof}

Nel caso particolare delle multidistribuzioni su numeri reali, possiamo parlare
di valore atteso.

\begin{definition}[Valore atteso di una multidistribuzione su numeri reali]
  Sia $\mu \in \mathcal{M}_{\leq 1}(\mathbb{R})$. Il valore atteso della
  multidistribuzione sui numeri reali $\mu$, $\mathbb{E}(\mu)$, è definito come:
  $$\mathbb{E}(\mu) := \sum_{p:a \in \mu}p \cdot a$$
\end{definition}
