\chapter{Ordini probabilisticamente monotoni}

Al termine dello scorso capitolo, abbiamo presentato una tecnica corretta e
completa per provare che un PTRS sia SAST: esibire una funzione di rango
probabilistica per esso. In questo ultimo capitolo facciamo leva su questo
risultato per fornire una nuova tecnica di costruzione di PARS SAST a partire da
ARS che rispettano determinate condizioni.

\section{Motivazione}

Dato un generico PARS che vogliamo dimostrare essere SAST, la nostra prova
costruttiva del \cref{th:lyapunov-completeness} ci suggerirebbe di fare
affidamento sull'altezza attesa della derivazione. Dimostrare che essa
sia davvero una funzione di Lyapunov, però, non è sempre impresa facile nel caso
generale.

Questo problema ha una controparte non-probabilistica, da cui prenderemo
ispirazione: è la ricerca di ordini di semplificazione, e cioè ordini stretti di
riscrittura che soddisfano la seguente proprietà del sottotermine.

\begin{definition}[Proprietà del sottotermine]
  Sia $S \subseteq T^2(\Sigma, V)$. Si dice che $S$ gode della proprietà del
  sottotermine quando vale che
  $$\forall t \in T(\Sigma, V), p \in \mathcal{P}os(t) \setminus {\epsilon}
  \quad t S t|_p$$
\end{definition}

In assenza di effetti probabilistici, proviamo che un TRS sia terminante
mostrando che la sua relazione di riscrittura sia un ordine di semplificazione
secondo la definizione di cui sotto; in presenza di effetti probabilistici,
invece, proviamo che un PTRS sia SAST semplicemente costruendo una sua codifica
in $[\geq \epsilon + \mathbb{E}]$.

\begin{definition}[Ordine di semplificazione]
  Sia $S \subseteq T^2(\Sigma, V)$. Si dice che $S$ è un ordine di
  semplificazione su $T(\Sigma, V)$ quando $S$ è un ordine stretto su
  $T(\Sigma, V)$ che gode della proprietà del sottotermine.
\end{definition}

Entrambi gli approcci forniscono insomma la garanzia di terminazione nei loro
rispettivi ambiti. Si potrebbe quindi pensare, sulla base di questa analogia, di
espandere le definizioni di specifici ordini di semplificazione al contesto
probabilistico, e adattare di conseguenza le tencniche per la loro costruzione.
Questa strategia ha già portato i suoi frutti: Avanzini et al.\ \cite{avanzini}
hanno effettuato questa operazione per il metodo di interpretazione, mostrandone
i risultati per gli ordini di semplificazione polinomiali e matriciali.

In questo capitolo seguiremo invece una direttrice più ingenua: la costruzione
di tecniche che, applicate a generiche famiglie di ordini di semplificazione
noti nel rispetto di certe precondizioni, portino a famiglie di PTRS SAST.

\section{Definizione}

\subsection{Ordini probabilisticamente monotoni}

L'idea degli ordini probabilisticamente monotoni è quella di partire da un
ordine non probabilistico $\succ$ e limitare inferiormente con un valore
strettamente positivo la probabilità che ad ogni passo di computazione un
termine venga riscritto in uno con esso messo in relazione da $\succ$. Ecco gli
accorgimenti che ci guidano verso la loro definizione:

\begin{itemize}
  \item imporremo che le multidisitribuzioni proprie di arrivo siano costruite a
    partire da insiemi finiti di indici. Questa non è un'assunzione forte:
    essendo i PTRS che vogliamo catturare tipicamente finiti, ed essendoci
    pertanto un numero finito di applicazioni di regola a partire da un dato
    termine, la seguente definizione fungerà comunque da ottimo prototipo di
    relazione di riscrittura probabilistica alla base di un possibile PTRS;
  \item in vista di una futura funzione di rango probabilistico, e cioè codifica
    verso $[\geq \epsilon + \mathbb{E}]$, vogliamo vincolare inferiormente la
    decrescita della lunghezza di derivazione attesa lungo un passo di
    riduzione. Per fare ciò, dovremo imporre valori minimi anche ad almeno una
    probabilità;
  \item la nostra definizione di (sotto)multidistribuzione tollera anche
    probabilità nulle. È desiderabile che la nostra costruzione si comporti
    indifferentemente fra due multidistribuzioni che differiscono solo per la
    presenza di esiti caratterizzati da tali probabilità degeneri.
\end{itemize}

Segue un primo tentativo di definizione.

\begin{definition}[Ordine probabilisticamente monotono]
  Siano $\succ$ un sistema di riduzione astratto su $A$ tale che
    $$\forall a \in A \quad dh_{\succ}(a) \in \mathbb{N}$$
  e $\epsilon \in \mathbb{R}_{> 0}$. L'ordine probabilisticamente monotono (o
  PMO, \emph{probabilistically monotonic order}) indotto da $\succ$,
  $>_{p\succ}$, è il sistema di riduzione astratto probabilistico su $A$ che ha
  per elementi tutte e sole le coppie della forma
  $$(a, \{\!\!\{ p_i : b_i \mathbin{|} i \in I\}\!\!\})$$
  (e cioè i cui secondi elementi sono sempre multinsiemi finiti da insiemi
  finiti di indici) che rispettano la seguente condizione:
  $$\forall i \in I \quad (p_i = 0 \vee a \succeq b_i)$$
  $$\wedge$$
  $$\exists J \subseteq I \quad (\sum_{j \in J} p_j \geq \epsilon \quad \wedge
  \quad \forall j \in J \quad a \succ b_j)$$
\end{definition}

Il primo congiunto del precedente predicato esprime una proprietà che deve
essere condivisa da ogni elemento della multidistribuzione di arrivo: il
rispettare in modo non stretto l'ordine di partenza. Ovviamente, gli esiti che
si verificano con probabilità nulla sono esentati da questo vincolo. Il secondo
congiunto, invece, asserisce l'esistenza di un sottogruppo di esiti che si
faccia carico di ``diminuire'' il valore atteso durante un passo di riduzione,
esso potrebbbe essere composto perfino da un unico elemento o dall'intero
insieme $I$, ma di certo non può essere l'insieme vuoto, siccome abbiamo scelto
$\epsilon \in \mathbb{R}_{>0}$.

La seguente semplice implementazione in Standard ML fa leva sulla nozione che
nessuna probabilità assuma mai valori negativi. Se infatti esiste almeno un
$J \subseteq I$ che rispetti la condizione di cui sopra, allora essa sarà di
certo rispettata anche da $\{ j \in I \mathbin{|} a \succ b_j \}$ (\texttt{maxJ}
nel codice).

\smlrange{mpo.sml}{ordine probabilisticamente monotono}{5-16}

Prima di proseguire, ci concediamo il tempo di analizzare due esempi pratici di
(non-)applicazione di questo concetto.

\begin{example}[Sistema di riscrittura dei termini per $silly$]\label{ex:silly}
  Si consideri la procedura \texttt{silly} del seguente programma in Standard
  ML:

  \smlrange{geo.sml}{procedura \texttt{silly}}{1-12}

  Questo programma può essere descritto scegliendo $\Phi = \{0, S,
  silly\}$ (zero, funzione successore, procedura SML). Ovviamente $\Sigma(0) =
  0$, $\Sigma(S) = 1$ e $\Sigma(silly) = 1$. Il nostro PTRS $R$ avrà
  due sole regole, definite con l'ausilio della variabile $n$:

  $$silly(0) \quad R \quad \{\!\!\{1: 0\}\!\!\}$$

  $$silly(S(n)) \quad R \quad
  \left\{\!\!\left\{\frac{1}{2}: silly(n),
  \quad \frac{1}{2}: S(n)\right\}\!\!\right\}$$

  Costruiremo un ordine probabilisticamente monotono indotto dall'ARS $\succ$
  proposto a breve, che a sua volta fa uso della seguente definizione di
  ``altezza di un termine'' $t \in T(\Sigma, V)$, $h(t)$:
  \\~\\
  \begin{math}
    h(t) =
    \begin{cases}
      0 & \quad t \in V \vee t \in \Phi \wedge \Sigma(t) = 0 \\
      1 + \max_{i \in \{1, \dots, n\}} h(s_i) & \quad t = s(s_i, \dots, s_n)
    \end{cases}
  \end{math}
  \\~\\
  L'ARS su $T(\Sigma, V)$ denominato $\succ$ citato sopra è definito
  univocamente dalla seguente condizione:
  $$s \succ t \leftrightarrow h(s) > h(t)$$
  Quindi $>_{p\succ}$ è l'ordine probabilisticamente monotono indotto da
  $\succ$, e la relazione di riscrittura di $R$ è in effetti un sottoinsieme di
  $>_{p\succ}$. Di certo l'altezza di derivazione è, in questo caso, finita per
  ogni termine.
\end{example}

Il prossimo scenario è invece meno fortunato.

\begin{example}[Sistema di riscrittura dei termini per \texttt{geo}]\label{ex:geo}
  Si consideri la seguente procedura \texttt{geo}, ispirata a una successione
  geometrica:

  \smlrange{geo.sml}{procedura \texttt{geo}}{14-17}

  Aggiungendo ai valori assegnati a $\Phi$ e $\Sigma$ nell'esempio precedente
  $geo$, con $\Sigma(geo) = 1$, costruiamo stavolta il nostro PTRS R come un
  singoletto contenente solo la seguente regola, che fa sempre uso della
  variabile $n$:

  $$geo(n) \quad R \quad
  \left\{\!\!\left\{\frac{1}{2}: geo(S(n)),
  \quad \frac{1}{2}: n\right\}\!\!\right\}$$

  Affinché possiamo anche in questo caso concepire la relazione di riscrittura
  di $R$ come sottoinsieme di un PMO $>_{p\succ}$ indotto da un qualche ordine
  $\succ$, deve valere $geo(n) \succeq geo(S(n))$. Siccome $geo(n) \neq
  geo(S(n))$, in particolare, allora $geo(n) \succ geo(S(n))$. Questo
  garantirebbe l'esistenza di una catena discenente infinita $geo(n) \succ
  geo(S(n)) \succ geo(S(S(n))) \succ \dots$. Tale stato di cose, ovviamente, è
  incompatibile con il vincolo di finitezza delle altezze di derivazione dei
  termini.
\end{example}

La scomoda situazione appena descritta ci spinge a una riformulazione della
nostra definizione di ordine probabilisticamente monotono, che è attualmente
inadeguata per la gestione di queste casistiche.

\subsection{Ordini probabilisticamente monotoni via codifiche}

Lo studio dell'\cref{ex:geo} ci ha costretto a una scelta forzata: abbiamo
dovuto imporre $geo(n) \succ geo(S(n))$ solo perché $geo(n) \neq geo(S(n))$.
Tuttavia, guardando il nostro PTRS $R$, si potrebbe sostenere che $geo(n)$ e
$geo(S(n))$ abbiano la stessa altezza di derivazione attesa e che quindi, se
anche non sintatticamente uguali, dovrebbero essere considerati ``equivalenti''
dalla nostra definizione. La prima clausola della nostra nuova formulazione,
quindi, si basa su una funzione di codifica scalare dei termini per astrarre
dall'identità sintattica limitandosi a confrontare solo i valori delle codifiche
dei due termini in questione. Ovviamente, il ruolo inteso della codifica è
quello di rappresentare l'altezza di derivazione dell'ARS di partenza (o
quantomeno una grandezza strettamente monotona rispetto ad essa).

\begin{definition}[Ordine probabilisticamente monotono via codifica]\label{def:pmo-via-encoding}
  Siano $\succ$ un sistema di riduzione astratto su $A$, $\epsilon, \delta \in
  \mathbb{R}_{> 0}$ e $f: A \rightarrow \mathbb{R}_{>0}$ una codifica di $\succ$
  in $[\geq \delta +]$. L'ordine probabilisticamente monotono indotto da $\succ$
  via codifica $f$, $>_{p\succ,f}$, è il sistema di riduzione astratto
  probabilistico su $A$ che ha per elementi tutte e sole le coppie della forma
  $$(a, \{\!\!\{ p_i : b_i \mathbin{|} i \in I\}\!\!\})$$
  (e cioè i cui secondi elementi sono sempre multinsiemi finiti da insiemi
  finiti di indici) che rispettano la seguente condizione:
  $$\forall i \in I \quad (p_i = 0 \vee f(a) \succeq f(b_i))$$
  $$\wedge$$
  $$\exists J \subseteq I \quad (\sum_{j \in J} p_j \geq \epsilon \quad \wedge
  \quad \forall j \in J \quad a \succ b_j)$$
\end{definition}

Si noti che, ora che viene fatto usa della funzione di codifica, non è più
necessario imporre la finitezza dell'altezza di derivazione per ogni termine. La
nostra formalizzazione in Standard ML invece rimane pressoché invariata, con
l'aggiunta di un parametro funzionale che rappresenta $f$:

\smlrange{mpo.sml}{ordine probabilisticamente monotono}{18-23}

Ora possiamo con fiducia costruire un ARS ad hoc per \texttt{geo} che induca il
nostro PMO, tenendo però presente che questa volta necessiteremo anche di una
codifica.

\begin{example}[Sistema di riscrittura dei termini probabilistico quasi
  certamente terminante forte per \texttt{geo}]\label{ex:geo-2}
  Siano $\Phi$ un insieme, $\Phi' \subseteq \Phi$, $\Sigma$ una segnatura su
  $\Phi$, $V$ un insieme di variabili e $s \in T(\Sigma, V)$. Il numero di
  occorrenze di simboli in $\Phi'$ all'interno di $s$, $|s|_{\Phi'}$ è definito
  come:

  $$|s|_{\Phi'} := \begin{cases}
    0 & s \in V \\
    \sum_{i = 1}^n |s_i|_{\Phi'} &
      s = f(s_1, \dots, s_n) \wedge f \notin \Phi'\\
    1 + \sum_{i = 1}^n |s_i|_{\Phi'} &
      s = f(s_1, \dots, s_n) \wedge f \in \Phi'\\
  \end{cases}$$

  Gli ordini booleani che andiamo a definire si basano sul semplice confronto di
  questa quantità, che deve strettamente decrescere a ogni passo di riduzione.

  Siano $\Phi$ un insieme, $\Phi' \subseteq \Phi$, $\Sigma$ una segnatura su
  $\Phi$, $V$ un insieme di variabili e $s \in T(\Sigma, V)$. L'ordine booleano
  indotto da $\Phi'$, $>_{\Phi'}$, è l'unico ARS su $T(\Sigma, V)$ che soddisfa
  la seguente condizione:
  $$\forall s, t \in T(\Sigma, V) \quad
  (s >_{\Phi'} t \leftrightarrow |s|_{\Phi'} > |t|_{\Phi'})$$

  Partendo dall'\cref{ex:geo}, possiamo correttamente costruire il PMO indotto
  da $>_{\{geo\}}$ via codifica $|\cdot|_{\{geo\}}$.
\end{example}

\section{Terminazione quasi certa forte}

Dimostrare la terminazione quasi certa forte degli ordini probabilisticamente
monotoni (via codifica o meno) è, ovviamente, funzionale a provare che anche i
PTRS che hanno un PMO come relazione di riscrittura probabilistica siano SAST
(per la \cref{def:past-ptrs}). Perfino nel caso in cui la relazione di
riscrittura probabilistica in questione sia anche solo un sottoinsieme di un PMO
si ha la sicurezza che il PTRS in questione sia SAST: la funzione identità,
infatti, è di certo una codifica corretta dalla relazione di riscrittura verso
il PMO. Procediamo quindi a provare che ogni PMO sia SAST. Per farlo,
naturalmente, useremo lo strumento delle funzioni di rango probabilistiche, già
introdotte nel precedente capitolo. Per aiutarci a traslare i risultati di
terminazione in presenza di effetti probabilistici, faremo uso della seguente
definizione ausiliaria, sulla base della quale costruiremo poi l'unico lemma
necessario alla dimostrazione.

\begin{definition}[Controparte probabilistica di un sistema di riduzione
  astratto]
  Sia $\rightarrow$ un sistema di riduzione astratto su $A$. Definiamo la sua
  controparte probabilistica, $\rightarrowtail$, come il seguente sistema di
  riduzione astratto probabilistico su $A$:

  $$\rightarrowtail := \{ (a, \mu) \in A \times \mathcal{M}(A) \mathbin{|}
  \exists b \in A \quad
  (\mu = \{\!\!\{ 1 : b \}\!\!\} \wedge a \rightarrow b)\}$$
\end{definition}

Il seguente lemma garantisce che le controparti probabilistiche degli ordini
di percorso lessicografici siano SAST se e solo se tali ordini abbiamo
solo altezze di derivazione finite. Ai fini della nostra esposizione, tali
controparti fungono quindi da ``nucleo'' di partenza per provare SAST su tutti
gli ordini probabilisticamente monotoni indotti da ARS con altezze di
derivazione esclusivamente finite.

\begin{lemma}[Funzioni di Lyapunov delle controparti probabilistiche dei
  sistemi di riduzione astratti]
  Sia $\rightarrow$ un sistema di riduzione astratto su $A$ e $\epsilon \in
  \mathbb{R}_{>0}$. Una generica funzione $f: A \rightarrow \mathbb{R}_{>0}$ è
  una codifica di $\rightarrow$ in $[\geq \epsilon +]$ se e solo se $f$ è una
  codifica della controparte probabilistica di $\rightarrow$, $\rightarrowtail$,
  in $[\geq \epsilon + \mathbb{E}]$.
\end{lemma}

\begin{proof}
  Siccome $f(a_i) = f(\{\!\!\{1: a_i\}\!\!\})$ e $f(\varnothing) = 0$ le due
  nozioni di monotonia diventano equivalenti.
\end{proof}

In realtà, la controparte probabilistica di un ARS è essa stessa un caso
degenere di ordine di ordine probabilisticamente monotono: basta fissare
$\epsilon = 1$ e, in tutte le coppie della relazione ottenuta, $J = I$. Partire
dal loro studio è quindi il passo più naturale verso il caso generale.

Partiamo dagli PMO via codifica: ci concentreremo in seguito su quelli semplici.

\begin{theorem}[Terminazione quasi certa forte degli ordini probabilisticamente
  monotoni via codifica]\label{th:main-theorem}
  Sia $\succ$ un sistema di riduzione astratto su $A$ e $\delta \in
  \mathbb{R}_{>0}$. L'ordine probabilisticamente monotono indotto da
  $\succ$ via codifica $f: A \rightarrow \mathbb{R}_{>0}$ di $\succ$
  in $[\geq \delta +]$, $>_{p\succ,f}$, è quasi certamente terminante forte.
\end{theorem}

\begin{proof}
  Si consideri $\sqsupset$, la controparte probabilistica di $\succ$. Per il
  precedente lemma, $f$ codifica $\sqsupset$ in $[\geq \delta + \mathbb{E}]$.
  Sia inoltre $\epsilon \in \mathbb{R}_{>0}$ la costante usata per definire
  $>_{p\succ,f}$. Per chiusura di $\mathbb{R}_{>0}$ rispetto alla
  moltiplicazione, $\epsilon \delta \in \mathbb{R}_{>0}$. Quindi $[\geq \epsilon
  \delta + \mathbb{E}]$ rispetta il vincolo di positività stretta della relativa
  definizione. Consideriamo ora un generico elemento di $>_{p\succ,f}$, che
  indicheremo con $(a, \{\!\!\{p_i : b_i \mathbin{|} i \in I\}\!\!\})$ (per
  definizione di $>_{p\succ,f}$, la multidistribuzione ha necessariamente questa
  forma, e non abbiamo quindi perdita di generalità):
  \begin{enumerate}
    \item sia $i \in I$. Il primo congiunto della condizione espressa da
      $>_{p\succ,f}$ richiede che $p_i = 0 \vee f(a) \geq f(b_i)$. In entrambi i
      casi, osserviamo che quindi vale $p_i f(a) \geq p_i f(b_i)$ (siccome
      $p_i \geq 0$).
      % \begin{itemize}
      %   \item se $p_i = 0$, allora la disuguaglianza è banalmente soddisfatta;
      %   \item se $a \succeq b_i$, allora procediamo di nuovo per casi:
      %     \begin{itemize}
      %       \item se $a \succ b_i$, allora $a \sqsupset \{\!\!\{1:
      %         b_i\}\!\!\}$. Essendo $f$ una codifica di $\sqsupset$ in $[\geq
      %         \delta + \mathbb{E}]$, si ha che $f(a) \geq \delta +
      %         \mathbb{E}(f(\{\!\!\{1 : b_i\}\!\!\}))$ e perciò $f(a) \geq \delta
      %         + f(b_i)$. Essendo $\delta \in \mathbb{R}_{>0}$, di certo
      %         $f(a) \geq f(b_i)$. Infine, siccome $p_i \geq 0$, la
      %         disuguaglianza iniziale è verificata;
      %       \item se $a = b_i$, invece, la verifica è ovvia;
      %     \end{itemize}
      % \end{itemize}
    \item sia $J \subseteq I$ un insieme che soddisfa il secondo congiunto della
      condizione espressa da $>_{p\succ,f}$. Quindi $\sum_{j \in J} p_j \geq
      \epsilon \wedge \forall j \in J \quad a \succ b_j$. Fissiamo un generico
      indice $j \in J$. Da $a \succ b_j$ segue $a \sqsupset \{\!\!\{1 : b_j
      \}\!\!\}$. Sapendo che $f$ è una codifica di $\sqsupset$ in $[\geq
      \delta + \mathbb{E}]$, otteniamo $f(a) \geq \delta +
      \mathbb{E}(f(\{\!\!\{1 : b_j\}\!\!\}))$ e perciò $f(a) \geq \delta +
      f(b_j)$. Poiché $p_j > 0$, vale che $p_j f(a) \geq p_j \delta +
      p_j f(b_j)$. Per ogni possibile $j \in J$, sommiamo membro a membro le
      istanze di quest'ultima disuguaglianza e otteniamo $\sum_{j \in J} (p_j
      f(a)) \geq \sum_{j \in J} (p_j \delta) + \sum_{j \in J} (p_j f(b_j))$.
      Per il primo congiunto della condizione imposta da $>_{p\succ,f}$,
      $\sum_{j \in J} p_j \geq \epsilon$ e, siccome $\delta \in
      \mathbb{R}_{>0}$, allora $(\sum_{j \in J} p_j) \delta \geq \epsilon
      \delta$. Quindi $\sum_{j \in J} (p_j f(a)) \geq \epsilon \delta + \sum_{j
      \in J} (p_j f(b_j))$.
  \end{enumerate}
  Sommando membro a membro la disuguaglianza (2) con tutte le istanze della
  (1) per ogni $i \in I \setminus J$, si ottiene:
  $$\sum_{j \in J} (p_j f(a)) + \sum_{i \in I \setminus J}(p_i f(a)) \geq
  \epsilon \delta + \sum_{j \in J}(p_j f(b_j)) + \sum_{i \in I \setminus J}(p_i
  f(b_i))$$
  $$\sum_{i \in I}(p_i f(a)) \geq \epsilon \delta + \sum_{i \in I}(p_i f(b_i))$$
  $$f(a) \sum_{i \in I}p_i \geq \epsilon \delta + \sum_{i \in I}(p_i f(b_i))$$
  Ricordiamo che $>_{p\succ,f}$ è un PARS, e come tale i secondi membri dei suoi
  elementi sono sempre multidistribuzioni proprie ($\sum_{i \in I} p_i = 1$):
  $$f(a) \geq \epsilon \delta + \sum_{i \in I}(p_i f(b_i)) =
  \epsilon \delta + \mathbb{E}(f(\{\!\!\{p_i : b_i \mathbin{|} i \in I
  \}\!\!\}))$$
  Quindi $f$ è una codifica di $>_{p\succ,f}$ in $[\geq \epsilon \delta +
  \mathbb{E}]$, e cioè una funzione di Lyapunov. Perciò $>_{p\succ,f}$ è quasi
  certamente terminante forte.
\end{proof}

Ottenuto questo importante risultato, la terminazione quasi certa forte degli
ordini probabilistici monotoni senza codifica segue come corollario.

\begin{corollary}[Terminazione quasi certa forte degli ordini
  probabilisticamente monotoni senza codifica]\label{cor:main-corollary}
  Sia $\succ$ un sistema di riduzione astratto su $A$ tale che
  $$\forall a \in A \quad dh_{\succ}(a) \in \mathbb{N}$$. L'ordine
  probabilisticamente monotono indotto da $\succ$ in $[\geq \delta +]$,
  $>_{p\succ}$, è quasi certamente terminante forte.
\end{corollary}

\begin{proof}
  Siccome $\forall a \in A \quad dh_{\succ}(a) \in \mathbb{N}$ e inoltre
  $\forall a,b \in A \quad (a \succeq b \rightarrow dh_{\succ}(a) \geq
  dh_{\succ}(b))$, $dh_{\succ}$ è di certo una codifica di $A$ in $[\geq 1 +]$
  e inoltre, scelta per $>_{p\succ,dh_{\succ}}$ la stessa costante $\epsilon \in
  \mathbb{R}_{>0}$ usata per definire $>_{p\succ}$, vale che:
  $$\forall a,b \in A \quad (a >_{p\succ} b \rightarrow
  a >_{p\succ,dh_{\succ}} b)$$
  Dal \cref{th:main-theorem}, poi, segue che $>_{p\succ,dh_{\succ}}$ è SAST.
  Quindi, anche $>_{p\succ}$, che ne è un sottoinsieme, lo è.
\end{proof}

\section{Esempio di non-applicazione: gli ordini di percorso lessicografici}

L'unico vero limite della \cref{def:pmo-via-encoding} è la necessarietà di una
codifica in $[\geq \delta +]$. In altre parole, è richiesto partire sempre da
ordini le cui controparti probabilistiche siano SAST. In questo paragrafo,
esibiamo una famiglia di ordini di semplificazione estremamente popolari (gli
ordini di percorso lessicografici) e mostriamo che, non essendo SAST, essi non
inducono ordini probabilisticamente monotoni (con o senza codifica).

Gli ordini di percorso lessicografici appartengono alla famiglia degli ordini
di percorso ricorsivi, che confrontano dapprima i simboli alla radice dei due
termini in questione e poi, ricorsivamente, i loro sottotermini immediati. In
particolare, gli ordini di percorso lessicografici, come suggerisce il nome,
considerano i sottotermini immediati come tuple ordinate i cui i-esimi elementi
diventano rilevanti solo a parità di tutti quelli che li precedono. Se invece
avessimo scelto di prescindere dall'ordine dei sottotermini, avremmo potuto
effettuare uno studio degli ordini di percorso con multinsiemi.

\begin{definition}[Ordine di percorso lessicografico]\label{def:lpo}
  Siano $\Phi$ un insieme finito di simboli di funzione, $\Sigma$ una segnatura
  per $\Phi$, $V$ un insieme di variabili e $>$ un ordine stretto su $\Sigma$.
  L'ordine di percorso lessicografico (o LPO, \emph{lexicographic path order})
  su $T(\Sigma, V)$ indotto da $>$, $>_{lpo}$, è la relazione su $T(\Sigma, V)$
  che rispetta la seguente condizione ricorsiva lessicografica su $(s, t)$:

  $\forall s,t \in T(\Sigma, V), s >_{lpo} t \iff$

  \textbf{(LPO1)} \quad $t \in \mathcal{V}ar(s) \quad \wedge \quad s \neq t \quad \vee$

  \textbf{(LPO2)} \quad $s = f(s_1, \dots, s_m) \quad \wedge \quad
                t = g(t_1, \dots, g_n) \quad \wedge$

  \textbf{(LPO2a)} \quad \quad $(\exists i \in \{1, \dots m\} \quad
                                s_i \geq_{lpo} t \quad \vee$

  \textbf{(LPO2b)} \quad \quad $f > g \quad \wedge \quad
                                \forall j \in \{1, \dots, n\} \quad
                                s >_{lpo} t_j \quad \vee$

  \textbf{(LPO2c)} \quad \quad $f = g \quad \wedge \quad
                                \forall j \in \{1, \dots, n\} \quad
                                s >_{lpo} t_j \quad \wedge$

  \quad \quad \quad \quad \quad \quad \quad $\exists i \in \{1, \dots, m\} \quad
                                (s_i >_{lpo} t_i \quad \wedge \quad
                                \forall k \in \{1, \dots, i - 1\} \quad
                                s_k = t_k))$
\end{definition}

Si osserva che:
\begin{itemize}
  \item $\geq_{lpo}$ è il quasi-ordine che consiste nella chiusura riflessiva di
  $>_{lpo}$: non potrebbe ovviamente essere l'ordine di percorso lessicografico
  indotto da $\geq$, che non è stretto;
  \item la ricorsione di cui sopra è ben definita, siccome usata solo su coppie
  di termini lessicograficamente più ``piccole'' di $(s, t)$.
\end{itemize}

Per formalizzare questo concetto, Baader e Nipkow \cite{baader} partono dal
presentare un'implementazione per un generico ordine lessicografico.

\smlrange{traat/orders.ML}{ordine lessicografico}{14-21}

Risulta necessaria anche la seguente definizione di \texttt{forall}.

\smlrange{traat/library.ML}{quantificatore universale per liste}{22-24}

L'ordine risultante è ovviamente definito sulle istanze di \texttt{term}, il
tipo precedentemente definito:

\smlrange{traat/termorders.ML}{ordine di percorso lessicografico}{17-31}

Come già anticipato, gli ordini di percorso lessicografici sono inadatti alla
costruzione di un qualsiasi tipo di ordini probabilisticamente monotoni.

\begin{theorem}[Esistenza di ordini di percorso lessicografici che non
  inducono ordini probabilisticamente monotoni via codifica]
  Esiste almeno un ordine di percorso lessico che non induce nessun ordine
  probabilisticamente monotono via codifica.
\end{theorem}
  
\begin{proof}
  Consideriamo, sempre nel contesto dell'\cref{ex:geo-2}, le seguenti procedure:
  \smlrange{expwait.sml}{procedure $\texttt{double}$, $\texttt{exp}$ e
  $\texttt{wait}$}{3-13}
  Possiamo formalizzarle aggiungendo $double$, $exp$ e $wait$ a $\Phi$, con
  $\Sigma(double) = \Sigma(exp) = \Sigma(wait) = 1$. Aggiungiamo ora opportune
  nuove regole per descrivere il comportamento di questi nuovi simboli di
  funzione. Come di consueto, facciamo affidamento alla variabile ausiliaria
  $n$. Il simbolo di funzione $double$ rappresenta il doppio del numero naturale
  passatogli come argomento:
  $$double(0) \quad R \quad \{\!\!\{1: 0\}\!\!\}$$
  $$double(S(n)) \quad R \quad \{\!\!\{1: S(S(double(n)))\}\!\!\}$$
  Il simbolo di funzione $exp$ rappresenta la potenza di due che ha
  per esponente il numero naturale passatogli come argomento:
  $$exp(0) \quad R \quad \{\!\!\{1: S(0)\}\!\!\}$$
  $$exp(S(n)) \quad R \quad \{\!\!\{1: double(exp(n))\}\!\!\}$$
  Infine, il simbolo di funzione $wait$ non rappresenta un valore particolare
  (la scelta di $0$ nel programma Standard ML è solo un dettaglio
  implementativo). Ciò che è interessante è che $wait(n)$ venga portato
  in forma normale dopo esattamente $n + 1$ passi di riduzione:
  $$wait(0) \quad R \quad \{\!\!\{1: 0\}\!\!\}$$
  $$wait(S(n)) \quad R \quad \{\!\!\{1: wait(n)\}\!\!\}$$
  Consideriamo ora un qualche ordine $>$ su $\Phi$ tale che $exp > double > S$.
  Chiamiamo l'ordine lessicografico da esso indotto $>_{lpo}$. Assumiamo che
  esso, a sua volta, induca un ordine probabilisticamente monotono $>_{plpo}$
  via codifica e ci riproponiamo di ridurci all'assurdo. Usando la
  \cref{def:lpo} e la \cref{def:pmo-via-encoding}, è facile verificare che tutte
  le regole fino a qui presentate (incluse quelle che descrivono $geo$)
  appartengono a $>_{plpo}$. Quindi esistono sequenze di riduzione
  probabilistica ammesse da $>_{plpo}$ che rispettano la seguente forma:
  $$
  \mu_n =
  \begin{cases}
    \left\{\!\!\left\{1: wait(exp(geo(0)))\right\}\!\!\right\} &
    n = 0 \\
    \left\{\!\!\left\{2^{-n}: wait(exp(geo(n))),
    2^{-n}: wait(exp(n - 1)),
    \dots\right\}\!\!\right\} &
    n > 0 \\
  \end{cases}
  $$
  Per ottenerle, infatti, è sufficiente considerare a ogni passo di riduzione
  probabilistica l'applicazione dell'unica regola già citata specifica per
  $geo$. Possiamo stimare per difetto la lunghezza di derivazione attesa di
  un generico elemento $\mu$ di questa famiglia di sequenze: per ogni termine
  della sequenza con indice $n > 0$ possiamo contare sull'esistenza di un
  elemento $(2^{-n}: wait(exp(n - 1)))$, che causerà a cascata almeno altri
  $2^{n - 1}$ elementi caratterizzati dalla stessa probabilità $2^{-n}$. Quindi:
  $$edl_{>_{plpo}}(\mu) = \sum_{n \in \mathbb{N}} |\mu_i| \geq
  \sum_{n \in \mathbb{N}_{>0}} (2^{-n} 2^{n - 1}) =
  \sum_{n \in \mathbb{N}_{>0}} \frac{1}{2} = +\infty$$
  Il PTRS in questione, quindi, non è PAST e, a maggior ragione, non è perciò di
  certo neanche SAST. Essendo esso un ordine probabilisticamente monotono,
  questo è in contraddizione con il \cref{th:main-theorem}. Assurdo.
\end{proof}

Ovviamente, questo risultato si applica anche in assenza di codifiche.

\begin{corollary}[Esistenza di ordini di percorso lessicografici che non
  inducono ordini probabilisticamente monotoni senza codifica]
  Esiste almeno un ordine di percorso lessico che non induce nessun ordine
  probabilisticamente monotono senza codifica.
\end{corollary}

\begin{proof}
  Per il precedente teorema, esiste un ordine di percorso lessicografico che non
  induce ordini probabilisticamente monotoni via codifica. Quindi il corollario
  è banalmente dimostrato, perché se esso inducesse un tale ordine
  probabilisticamente monotono senza codifica, potremmo costruirne
  uno via codifica in modo analogo a quando fatto nella prova del
  \cref{cor:main-corollary}, contraddicendoci.
\end{proof}

\section{Conclusioni}

Nel corso del nostro lavoro, abbiamo ripercorso le nozioni preliminari per
trattare i sistemi di riscrittura dei termini probabilistici. Abbiamo definito
gli ordini probabilisticamente monotoni, uno strumento per costruire sistemi di
riduzione astratti probabilistici a partire da sistemi di riduzione astratti
tradizionali. Per entramble le varianti (fra esse non equivalenti) proposte,
abbiamo provato che tutti gli ordini probabilisticamente monotoni sono quasi
certamente terminanti forti, con tanto di esempi applicativi. Abbiamo anche
ottenuto costruttivamente un risultato negativo: non tutti gli ordini di
percorso lessicografici sono adatti a indurre ordini probabilisticamente
monotoni di un qualche tipo.
